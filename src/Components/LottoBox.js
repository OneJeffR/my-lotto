import React, { Component } from "react";
import { Button } from "reactstrap";

class LottoBox extends Component {
  constructor(props) {
    super(props)

    this.state = {
      selected: false,
      disabled: false
    }

    this.onCheckboxBtnClick = this.onCheckboxBtnClick.bind(this)
  }

  onCheckboxBtnClick() {
    let newSelectedState = !(this.state.selected) //javascript läuft manchmal asynchron, deshalb zwischen Variable einbauen
    this.setState({ selected: newSelectedState })
    if (newSelectedState) {
      // wenn true ist, dann wird handleSelect ausgeführt
      this.props.onNumberSelect(this.props.number)
    }
    else {
      // wenn false ist, dann wird handleUnselect ausgeführt
      this.props.onNumberUnselect(this.props.number)
    }
  }

  render() {
    return (
      <div>
        <Button
          color={this.getButtonColor()}
          onClick={() => this.onCheckboxBtnClick()}
          active={this.state.selected}
          disabled={this.props.disableNonActive && !(this.state.selected)} //wird nur deaktiviert wenn es nicht selektiert wurde
        >
          {this.props.number}
        </Button>
      </div>

    )
  }

  getButtonColor() {
    let colors = ""
    colors += this.state.selected === false ? "secondary" : "primary"
    return colors
  }
}

export default LottoBox
