import React, { Component } from "react";
import { Button } from "reactstrap"; // https://reactstrap.github.io/
import SkyLight from "react-skylight"; // http://marcio.github.io/react-skylight/
import LottoBox from "./LottoBox";

class LottoField extends Component {
  constructor(props) {
    super(props)

    this.state = {
      numbers: [],
      disableNonActive: false,
      disableContinue: true,
      isHidden: true
    }
  }

  checkMaxSixBoxes() {
    if (this.state.numbers.length >= 6) {
      // deaktiviere alle unselected Lottoboxen
      // aktiviere und zeige Weiter Button
      this.setState({ disableNonActive: true, disableContinue: false, isHidden: false })
    }
    else {
      // aktiviere alle unselected Lottoboxen
      // deaktiviere und verstecke Weiter Button
      this.setState({ disableNonActive: false, disableContinue: true, isHidden: true })
    }
  }

  handleSelect = (number) => {
    if (this.state.numbers.indexOf(number) === -1) {
      if (this.state.numbers.length < 6) {
        this.state.numbers.push(number)
        this.state.numbers.sort(function (a, b) { return a - b })
        this.setState({ numbers: [...this.state.numbers] }) // warum ?! array geändert ist praktisch ein neues Objekt
        this.checkMaxSixBoxes()
      }
    }
  }

  handleUnselect = (number) => {
    let index = this.state.numbers.indexOf(number)
    if (index > -1) {
      this.state.numbers.splice(index, 1)
      this.setState({ numbers: [...this.state.numbers] })
      this.checkMaxSixBoxes()
    }
  }

  formatNumbers() {
    let value = this.state.numbers.length
    return value === 0 ? "Keine" : JSON.stringify(this.state.numbers)
  }

  createField = () => {
    let field = []
    let k = 1
    // äußere Schleife um Reihen zu erschaffen
    for (let i = 1; i < 8; i++) {
      let children = []
      // innere Schleife um Spalten zu erschaffen
      for (let j = 1; j < 8; j++) {
        children.push(<td>{
          <LottoBox
            number={k}
            onNumberSelect={this.handleSelect} //methoden
            onNumberUnselect={this.handleUnselect}
            disableNonActive={this.state.disableNonActive}
          />}</td>)
        k = k + 1
      }
      // erschaffe Reihen und füge Spalten hinzu
      field.push(<tr>{children}</tr>)
    }
    return field
  }

  render() {
    return ( //kann keine for-schleife hier drin ausführen, deshalb createField() vorher
      <div>
        <p><h1>Lotto App</h1></p>

        <table>
          {this.createField()}
        </table>
        <br />

        {!this.state.isHidden &&
          <Button
            disabled={this.state.disableContinue}
            color="warning"
            onClick={() => this.simpleDialog.show()}
          >
            Weiter
        </Button>}
        <SkyLight hideOnOverlayClicked ref={ref => this.simpleDialog = ref} title="Bestätigung deiner Auswahl">
          <p>{this.formatNumbers()}</p>
        </SkyLight>

      </div>
    )
  }
}

export default LottoField